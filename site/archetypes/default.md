+++
title = "{{ replace .TranslationBaseName "-" " " | title }}"
date = {{ .Date }}

type = "page"
layout = "layout-1"

pageClass = ""

menu = "main"
weight = 1

draft = false
+++